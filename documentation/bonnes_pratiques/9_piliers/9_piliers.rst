
.. index::
   pair: 9-pillars-of-containers-best-practices ; Best practices



.. _9_pillars_of_containers_best_practices:

=============================================
9-pillars-of-containers-best-practices
=============================================

.. seealso::

   - https://containerjournal.com/2018/10/16/9-pillars-of-containers-best-practices/
