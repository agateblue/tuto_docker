

.. index::
   pair: Image ; Alpine
   pair: 3.9 ; Alpine


.. _images_alpine:

==============================
Images **Alpine**
==============================


.. seealso::

   - https://store.docker.com/images/alpine
   - https://hub.docker.com/_/alpine/
   - https://www.alpinelinux.org/
   - https://fr.wikipedia.org/wiki/Alpine_Linux
   - https://github.com/gliderlabs/docker-alpine
   - http://gliderlabs.viewdocs.io/docker-alpine/




.. figure:: alpinelinux-logo.svg
   :align: center

   Le logo Alpine-linux


.. figure:: images_alpine.png
   :align: center

   https://store.docker.com/images/alpine


Short Description
==================

A minimal Docker image based on Alpine Linux with a complete package
index and only 5 MB in size!


Description
============

.. seealso::

   - https://fr.wikipedia.org/wiki/Alpine_Linux

Alpine Linux est une distribution Linux ultra-légère, orientée sécurité
et basée sur Musl et BusyBox, principalement conçue pour "Utilisateur
intensif qui apprécie la sécurité, la simplicité et l'efficacité des
ressources".

Elle utilise les patches PaX et Grsecurity du noyau par défaut et compile
tous les binaires de l'espace utilisateur et exécutables indépendants de
la position (dits "portables") avec protection de destruction de la pile.

Cette distribution se prête particulièrement, en raison de sa légèreté,
à la création d'images de containers Docker.

La distribution Alpine Linux est particulièrement populaire pour
cet usage   .


Dockerfile
===========


3.9
----

.. seealso::

   - https://github.com/gliderlabs/docker-alpine/blob/c4f4c7a6e14d6efeb9a160da464717e03d2cc3ee/versions/library-3.9/x86_64/Dockerfile

::

    FROM scratch
    ADD rootfs.tar.xz /
    CMD ["/bin/sh"]
