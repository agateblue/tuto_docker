

.. index::
   pair: Docker ; Library
   pair: Docker ; Store
   pair: Docker hub ; Explore
   pair: Images ; Docker


.. _docker_library:
.. _store_docker:

============================================
Images Docker (Store Docker, ex Hub docker)
============================================


.. seealso::

   - https://store.docker.com/
   - https://docs.docker.com/engine/userguide/eng-image/
   - https://github.com/docker-library
   - https://github.com/docker-library/official-images
   - https://hub.docker.com/explore/


.. contents::
   :depth: 5



Nouveau: le docker store: https://store.docker.com/
=====================================================

.. figure:: docker_store.png
   :align: center

   https://store.docker.com/


Ancien: le hub docker https://hub.docker.com/explore/
=======================================================

.. figure:: docker_hub_explore.png
   :align: center

   https://hub.docker.com/explore/



Gitlab registry
===================

.. toctree::
   :maxdepth: 3

   gitlab_registry/gitlab_registry


Images OS
===========

.. toctree::
   :maxdepth: 3

   alpine/alpine
   debian/debian
   ubuntu/ubuntu
   centos/centos

Images langages
=================

.. toctree::
   :maxdepth: 3

   python/python
   pipenv/pipenv
   php/php
   ruby/ruby
   node/node
   golang/golang
   openjdk/openjdk



Images **webserver**: serveurs HTTP (serveurs Web)
====================================================

.. seealso::

   - https://fr.wikipedia.org/wiki/Serveur_HTTP
   - https://fr.wikipedia.org/wiki/Serveur_HTTP#Logiciels_de_serveur_HTTP
   - https://en.wikipedia.org/wiki/Web_server


Le serveur HTTP le plus utilisé est Apache HTTP Server qui sert environ
55% des sites web en janvier 2013 selon Netcraft.

Le serveur HTTP le plus utilisé dans les 1 000 sites les plus actifs est
en revanche Nginx avec 38,2% de parts de marché en 2016 selon w3techs
et 53,9% en avril 2017.


.. toctree::
   :maxdepth: 3

   apache_httpd/apache_httpd
   apache_bitnami/apache_bitnami
   apache_tomcat/apache_tomcat
   node/node

Images **webserver** : serveurs Web + reverse proxy + load balancer
-------------------------------------------------------------------------------

.. seealso::

   - https://en.wikipedia.org/wiki/Reverse_proxy
   - https://fr.wikipedia.org/wiki/Proxy_inverse
   - https://en.wikipedia.org/wiki/Load_balancer
   - https://en.wikipedia.org/wiki/HTTP_cache

.. seealso::

   - https://en.wikipedia.org/wiki/Web_server


Apache HTTP Server + mod_proxy
+++++++++++++++++++++++++++++++++

Apache HTTP Server, serveur HTTP libre configurable en :term:`proxy inverse`
avec le module mod_proxy.

Nginx
++++++


.. toctree::
   :maxdepth: 3


   nginx/nginx



Images **authentication** authentification
=============================================

.. toctree::
   :maxdepth: 3

   ldap/ldap


Images **db** : bases de données
=================================

.. toctree::
   :maxdepth: 3

   postgresql/postgresql
   mariadb/mariadb
   sybase/sybase



Images message queue
======================

.. toctree::
   :maxdepth: 3

   rabbitmq/rabbitmq


Images outils collaboratifs
=============================

.. toctree::
   :maxdepth: 3

   gitlab_community_edition/gitlab_community_edition
   redmine/redmine
   wordpress/wordpress


Images "documentation"
=========================

.. toctree::
   :maxdepth: 3

   miktex/miktex


Images outils scientifiques
=============================

.. toctree::
   :maxdepth: 3

   anaconda3/anaconda3


Images apprentissage
======================

.. toctree::
   :maxdepth: 3

   static_site/static_site
   hello_world/hello_world
