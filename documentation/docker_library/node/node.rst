

.. index::
   pair: Images ; Node


.. _images_node:

=====================================
Images **Node**
=====================================


.. seealso::

   - https://store.docker.com/images/node
   - https://en.wikipedia.org/wiki/Ruby_%28programming_language%29




.. figure:: node_logo.png
   :align: center

   Le logo Node.js


Short Description
==================

Node.js is a JavaScript-based platform for server-side and networking
applications.


What is Node.js ?
===================

.. seealso::

   - https://en.wikipedia.org/wiki/Node.js

Node.js is a software platform for scalable server-side and networking
applications.

Node.js applications are written in JavaScript and can be run within
the Node.js runtime on Mac OS X, Windows, and Linux without changes.

Node.js applications are designed to maximize throughput and efficiency,
using non-blocking I/O and asynchronous events.

Node.js applications run single-threaded, although Node.js uses multiple
threads for file and network events.

Node.js is commonly used for real-time applications due to its asynchronous nature.

Node.js internally uses the Google V8 JavaScript engine to execute code;
a large percentage of the basic modules are written in JavaScript.

Node.js contains a built-in, asynchronous I/O library for file, socket,
and HTTP communication.

The HTTP and socket support allows Node.js to act as a **web server**
without additional software such as Apache.
