

.. index::
   pair: Images ; MariaDB


.. _images_mariadb:

==============================
Images **MariaDB**
==============================


.. seealso::

   - https://store.docker.com/images/mariadb
   - https://en.wikipedia.org/wiki/MariaDB
   - https://fr.wikipedia.org/wiki/MariaDB




.. figure:: mariadb_logo.png
   :align: center

   Le logo MariaDB


.. figure:: images_mariadb.png
   :align: center

   https://store.docker.com/images/mariadb


Short Description
==================

MariaDB is a community-developed fork of MySQL intended to remain free
under the GNU GPL


What is MariaDB ?
===================

MariaDB is a community-developed fork of the MySQL relational database
management system intended to remain free under the GNU GPL.

Being a fork of a leading open source software system, it is notable
for being led by the original developers of MySQL, who forked it due to
concerns over its acquisition by Oracle.

Contributors are required to share their copyright with the MariaDB Foundation.

The intent is also to maintain high compatibility with MySQL, ensuring
a "drop-in" replacement capability with library binary equivalency and
exact matching with MySQL APIs and commands.
It includes the XtraDB storage engine for replacing InnoDB, as well as
a new storage engine, Aria, that intends to be both a transactional and
non-transactional engine perhaps even included in future versions of MySQL.


How to use this image
======================


Start a mariadb server instance

Starting a MariaDB instance is simple::

    $ docker run --name some-mariadb -e MYSQL_ROOT_PASSWORD=my-secret-pw -d mariadb:tag


where some-mariadb is the name you want to assign to your container,
my-secret-pw is the password to be set for the MySQL root user and tag
is the tag specifying the MySQL version you want.

See the list above for relevant tags.
Connect to MySQL from an application in another Docker container

Since MariaDB is intended as a drop-in replacement for MySQL, it can be
used with many applications.

This image exposes the standard MySQL port (3306), so container linking
makes the MySQL instance available to other application containers.
Start your application container like this in order to link it to the
MySQL container::

    $ docker run --name some-app --link some-mariadb:mysql -d application-that-uses-mysql
