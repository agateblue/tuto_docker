

.. index::
   pair: Image ; Centos
   pair: 7 ; Centos
   ! CentOS


.. _images_centos:

==============================
Images **CentOS**
==============================


.. seealso::

   - https://store.docker.com/images/centos
   - https://hub.docker.com/_/centos/
   - https://fr.wikipedia.org/wiki/CentOS
   - https://www.centos.org/



.. figure:: logo_centos.png
   :align: center

   Logo CentOS


.. figure:: images_centos.png
   :align: center

   https://hub.docker.com/_/centos/

Short Description
==================

The official build of CentOS.

Description
============

.. seealso::

   - https://fr.wikipedia.org/wiki/CentOS

CentOS (Community enterprise Operating System) est une distribution
GNU/Linux principalement destinée aux serveurs.
Tous ses paquets, à l'exception du logo, sont des paquets compilés à
partir des sources de la distribution RHEL (Red Hat Enterprise Linux),
éditée par la société Red Hat. Elle est donc quasiment identique à
celle-ci et se veut 100 % compatible d'un point de vue binaire.

Utilisée par 20 % des serveurs web Linux, elle est l'une des distributions
Linux les plus populaires pour les serveurs web. Depuis novembre 2013,
elle est la troisième distribution la plus utilisée sur les serveurs web ;
en avril 2017, elle était installée sur 20,6 % d’entre eux ; les
principales autres distributions étaient Debian (31,8 %), Ubuntu (35,8 %)
et Red Hat (3,3 %).


Structures
============

La RHEL en version binaire, directement installable et exploitable, ne
peut être obtenue que par achat d'une souscription auprès de Red Hat
ou de ses revendeurs.
La plupart des programmes inclus et livrés avec la Red Hat sont publiés
sous la licence GPL, qui impose au redistributeur (sous certaines conditions)
de fournir les sources. CentOS utilise donc les sources de la RHEL
(accessibles librement sur Internet) pour regénérer la Red Hat
à l'identique.

On peut donc considérer la CentOS comme une version gratuite de la
Red Hat. Le support technique est de type communautaire : il se fait
gratuitement et ouvertement via les listes de diffusion et les forums
de la communauté CentOS.

Depuis le 7 janvier 2014, Red Hat et CentOS se sont fortement rapprochées,
puisque la plupart des principaux membres maintenant la CentOS ont été
embauchés par Red Hat.


Versions
===========


centos 7
----------

.. seealso::

   - https://github.com/CentOS/sig-cloud-instance-images/blob/a77b36c6c55559b0db5bf9e74e61d32ea709a179/docker/Dockerfile
   - https://raw.githubusercontent.com/CentOS/sig-cloud-instance-images/a77b36c6c55559b0db5bf9e74e61d32ea709a179/docker/Dockerfile

::

    FROM scratch
    ADD centos-7-docker.tar.xz /

    LABEL org.label-schema.schema-version="1.0" \
        org.label-schema.name="CentOS Base Image" \
        org.label-schema.vendor="CentOS" \
        org.label-schema.license="GPLv2" \
        org.label-schema.build-date="20181205"

    CMD ["/bin/bash"]
