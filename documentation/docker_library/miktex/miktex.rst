

.. index::
   pair: Image ; MiKTeX
   pair: Miktex ; LaTeX
   ! LaTeX


.. _images_miktex:

==============================
Images **MiKTeX**
==============================


.. seealso::

   - https://store.docker.com/community/images/miktex/miktex
   - https://github.com/MiKTeX/docker-miktex
   - https://en.wikipedia.org/wiki/MiKTeX




Short Description
==================

MiKTeX is a distribution of the TeX/LaTeX typesetting system for Microsoft
Windows. It also contains a set of related programs.

MiKTeX provides the tools necessary to prepare documents using the
TeX/LaTeX markup language, as well as a simple tex editor: TeXworks.

The name comes from Christian Schenk's login: MiK for Micro-Kid.
