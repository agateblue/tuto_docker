
.. index::
   pair: Docker Compose; Production


.. _docker_compose_production:

==============================
Docker compose in production
==============================

.. seealso::

   - https://docs.docker.com/compose/production/
   - https://docs.docker.com/compose/production/#modify-your-compose-file-for-production





Articles
==========


Simple continuous deployment with docker compose, docker machine and Gitlab CI (2017-06-26)
------------------------------------------------------------------------------------------------

.. seealso::

   - https://medium.com/@Empanado/simple-continuous-deployment-with-docker-compose-docker-machine-and-gitlab-ci-9047765322e1


For local development of microservice-based systems running on docker,
we’ve found that docker compose is probably the best way to go for
local development, with the docker compose yaml file format being very
usable for configuration as well. And for some projects, there really
is no need to scale up to having multiple containers of a service, as
you’ll be just fine with running all your containers on a single host.
For these projects you want to get to production as smooth (or simple) as possible.

So after spending time learning about Mesos, Kubernetes, Amazon ECS and
other proprietary technologies and learning a ton of new concepts,
I concluded that they’re all awesome, but not really suitable for a
simple move from local development with docker compose.
They all have their own configuration formats (largely for good reasons)
and all of them do orchestration quite a bit different than docker compose,
to facilitate more complex deployment environments.
