

.. index::
   pair: Naming ; Containers
   pair: JSON ; inspect


.. _naming_containers:

====================================================
Naming and inspecting containers
====================================================

.. seealso::

   - https://avril2018.container.training/intro.yml.html#231
   - https://avril2018.container.training/intro.yml.html#10
   - :ref:`petazzoni`




Objectives
============

.. seealso::

   - https://avril2018.container.training/intro.yml.html#233

In this lesson, we will learn about an important Docker concept:
container naming.

Naming allows us to:

- Reference easily a container.
- Ensure unicity of a specific container.

We will also see the :ref:`inspect command <docker_inspect>`, which gives
a lot of details about a container.


Naming our containers
========================

.. seealso::

   - https://avril2018.container.training/intro.yml.html#234


So far, we have referenced containers with their ID.

We have copy-pasted the ID, or used a shortened prefix.

But each container can also be referenced by its name.

If a container is named thumbnail-worker, I can do:

::

	$ docker logs thumbnail-worker
	$ docker stop thumbnail-worker
	etc.


Default names
===============

.. seealso::

   - https://avril2018.container.training/intro.yml.html#235


When we create a container, if we don't give a specific name, Docker
will pick one for us.

It will be the concatenation of:

- A mood (furious, goofy, suspicious, boring...)
- The name of a famous inventor (tesla, darwin, wozniak...)

Examples: happy_curie, clever_hopper, jovial_lovelace ...


.. _specify_name:

Specifying a name
====================

.. seealso::

   - https://avril2018.container.training/intro.yml.html#236


You can set the name of the container when you create it.

::

    $ docker run --name ticktock jpetazzo/clock

If you specify a name that already exists, Docker will refuse to create
the container.

This lets us enforce unicity of a given resource.


.. _renaming_container:

Renaming containers
=====================

.. seealso::

   - https://avril2018.container.training/intro.yml.html#237


You can rename containers with :ref:`docker rename <docker_rename>`.
This allows you to "free up" a name without destroying the associated
container.


.. _inspecting_container:

Inspecting a container
===========================

.. seealso::

   - https://avril2018.container.training/intro.yml.html#238


The docker inspect command will output a very detailed JSON map.

::

    $ docker inspect <containerID>

::

	[{
	...
	(many pages of JSON here)
	...


There are multiple ways to consume that information


Parsing JSON with the Shell
==============================

.. seealso::

   - https://avril2018.container.training/intro.yml.html#239


- You could grep and cut or awk the output of docker inspect.
- Please, don't.
- It's painful.
- If you really must parse JSON from the Shell, use JQ! (It's great.)



::

    $ docker inspect <containerID> | jq .


We will see a better solution which doesn't require extra tools.



.. _inspect_format:

Using --format
==================

.. seealso::

   - https://avril2018.container.training/intro.yml.html#240


You can specify a format string, which will be parsed by Go's
text/template package.

::

    $ docker inspect --format '{{ json .Created }}' <containerID>
    "2015-02-24T07:21:11.712240394Z"


- The generic syntax is to wrap the expression with double curly braces.
- The expression starts with a dot representing the JSON object.
- Then each field or member can be accessed in dotted notation syntax.
- The optional json keyword asks for valid JSON output.
  (e.g. here it adds the surrounding double-quotes.)
