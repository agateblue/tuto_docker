

.. index::
   pair: Chapter6 ; Avril 2018


.. _chapter6_avril_2018:

====================================================
Chapter6 Avril 2018
====================================================

.. seealso::

   - https://avril2018.container.training/intro.yml.html#427
   - https://avril2018.container.training/intro.yml.html#12
   - :ref:`petazzoni`


.. toctree::
   :maxdepth: 4
