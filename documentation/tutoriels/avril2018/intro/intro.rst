

.. index::
   pair: Intro ; Avril 2018


.. _intro_avril_2018:

====================================================
Intro Avril 2018
====================================================

.. seealso::

   - https://github.com/jpetazzo/container.training
   - https://avril2018.container.training/intro.yml.html#1
   - https://avril2018.container.training/intro.yml.html#16
   - :ref:`petazzoni`


.. toctree::
   :maxdepth: 4

   overview/overview
   history/history
