

.. index::
   pair: machine ; docker


.. _cail_docker_machine:

====================================================
Managing hosts with Docker Machine
====================================================

.. seealso::

   - https://avril2018.container.training/intro.yml.html#420
   - https://avril2018.container.training/intro.yml.html#11
   - :ref:`petazzoni`
