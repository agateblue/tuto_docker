

.. index::
   pair: docker-compose.yml  ; deploy (Funkwhale)


.. _funkwhale_deploy_dockercompose:

===========================================
Funkwhale deploy docker-compose.yml
===========================================


.. seealso::

   - https://dev.funkwhale.audio/funkwhale/funkwhale





Layout
========

::

    tree -L 2

::

    .
    ├── api
    │   ├── compose
    │   ├── config
    │   ├── Dockerfile
    │   ├── funkwhale_api
    │   ├── install_os_dependencies.sh
    │   ├── manage.py
    │   ├── requirements
    │   ├── requirements.apt
    │   ├── requirements.pac
    │   ├── requirements.txt
    │   ├── setup.cfg
    │   └── tests
    ├── CHANGELOG
    ├── changes
    │   ├── changelog.d
    │   ├── __init__.py
    │   ├── notes.rst
    │   └── template.rst
    ├── CONTRIBUTING.rst
    ├── CONTRIBUTORS.txt
    ├── demo
    │   ├── env.sample
    │   ├── README.md
    │   └── setup.sh
    ├── deploy
    │   ├── apache.conf
    │   ├── docker-compose.yml
    │   ├── docker.funkwhale_proxy.conf
    │   ├── docker.nginx.template
    │   ├── docker.proxy.template
    │   ├── env.prod.sample
    │   ├── FreeBSD
    │   ├── funkwhale-beat.service
    │   ├── funkwhale_proxy.conf
    │   ├── funkwhale-server.service
    │   ├── funkwhale.target
    │   ├── funkwhale-worker.service
    │   └── nginx.template
    ├── dev.yml
    ├── docker
    │   ├── nginx
    │   ├── ssl
    │   ├── traefik.toml
    │   └── traefik.yml
    ├── docs
    │   ├── api.rst
    │   ├── architecture.rst
    │   ├── build_docs.sh
    │   ├── build_swagger.sh
    │   ├── changelog.rst
    │   ├── configuration.rst
    │   ├── conf.py
    │   ├── contributing.rst
    │   ├── developers
    │   ├── Dockerfile
    │   ├── features.rst
    │   ├── federation
    │   ├── importing-music.rst
    │   ├── index.rst
    │   ├── installation
    │   ├── Makefile
    │   ├── serve.py
    │   ├── swagger.yml
    │   ├── third-party.rst
    │   ├── translators.rst
    │   ├── troubleshooting.rst
    │   ├── upgrading
    │   └── users
    ├── front
    │   ├── babel.config.js
    │   ├── Dockerfile
    │   ├── locales
    │   ├── package.json
    │   ├── public
    │   ├── scripts
    │   ├── src
    │   ├── stats.json
    │   ├── tests
    │   ├── vue.config.js
    │   └── yarn.lock
    ├── LICENSE
    ├── pyproject.toml
    ├── README.rst
    ├── scripts
    │   ├── clean-unused-artifacts.py
    │   └── set-api-build-metadata.sh
    └── TRANSLATORS.rst

    27 directories, 61 files




docker-compose.yml
=====================

.. literalinclude:: docker-compose.yml
   :linenos:



env.prod.sample
=====================


.. literalinclude:: env.prod.sample
   :linenos:
