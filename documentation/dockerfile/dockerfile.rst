
.. index::
   pair: Dockerfile ; Docker
   pair: Dockerfile ; MAINTAINER (deprecated)
   pair: MAINTAINER ; deprecated


.. _dockerfile:

=======================
Dockerfile
=======================

.. seealso::

   - https://docs.docker.com/engine/reference/builder/
   - https://docs.docker.com/engine/deprecated/

.. toctree::
   :maxdepth: 3

   multi_staging/multi_staging
