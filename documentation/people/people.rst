
.. index::
   pair: docker ; people

.. _docker_people:

==============================
Docker people
==============================

.. toctree::
   :maxdepth: 3

   bret_fischer/bret_fischer
   jpetazzo/jpetazzo
   nick_janetakis/nick_janetakis
   michael_bright/michael_bright
   sebastian_ramirez/sebastian_ramirez
   stephane_beuret/stephane_beuret
