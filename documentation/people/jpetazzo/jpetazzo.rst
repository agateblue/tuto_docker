
.. index::
   pair: jpetazzo ; people
   ! Jérôme Petazzoni

.. _docker_jpetazzo:

==================================================
Jérôme Petazzoni (https://twitter.com/jpetazzo)
==================================================

.. seealso::

   - https://twitter.com/jpetazzo
   - http://jpetazzo.github.io/
   - https://github.com/jpetazzo
   - https://github.com/jpetazzo/container.training
   - https://github.com/enix
   - https://github.com/enix/container.training
   - http://container.training
