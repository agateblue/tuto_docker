

.. index::
   pair: Nick ;Janetakis
   pair: Best practices ; Nick Janetakis
   ! Nick Janetakis


.. _nick_janetakis:

==============================
Nick Janetakis
==============================

.. seealso::

   - https://github.com/nickjj
   - https://galaxy.ansible.com/nickjj
   - https://twitter.com/nickjanetakis
   - https://nickjanetakis.com/blog/

.. figure:: nick_janetakis.jpeg
   :align: center


Best practices
================

.. seealso::

   - :ref:`docker_best_practices_nick`
