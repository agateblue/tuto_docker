.. index::
   pair: Docker ; Compose-switch
   ! Compose-switch

.. _compose_switch:

==============================
**compose switch**
==============================


- https://github.com/docker/compose-switch
- https://docs.docker.com/compose/cli-command/#compose-switch
- :ref:`docker_compose_2_0_0`


Description
============

Compose Switch is a replacement to the Compose V1 docker-compose (python)
executable.

It translates the command line into Compose V2 docker compose then run
the latter.


Manual installation
===========================

download compose-switch binary for your architecture
------------------------------------------------------

::

    $ curl -fL https://github.com/docker/compose-switch/releases/latest/download/docker-compose-linux-amd64 -o /usr/local/bin/compose-switch

make compose-switch executable
---------------------------------

::

    $ chmod +x /usr/local/bin/compose-switch

rename docker-compose binary if you already have it installed as /usr/local/bin/docker-compose
--------------------------------------------------------------------------------------------------

::

    $ mv /usr/local/bin/docker-compose /usr/local/bin/docker-compose-v1

define an "alternatives" group for docker-compose command
--------------------------------------------------------------

::

    $ update-alternatives --install /usr/local/bin/docker-compose docker-compose <PATH_TO_DOCKER_COMPOSE_V1> 1
    $ update-alternatives --install /usr/local/bin/docker-compose docker-compose /usr/local/bin/compose-switch 99

check installation
-----------------------

::

    $ update-alternatives --display docker-compose

::


    docker-compose - auto mode
      link best version is /usr/local/bin/compose-switch
      link currently points to /usr/local/bin/compose-switch
      link docker-compose is /usr/local/bin/docker-compose
    /usr/bin/docker-compose - priority 1
    /usr/local/bin/compose-switch - priority 99
