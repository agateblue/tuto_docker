
.. index::
   pair: News ; 2018-04


.. _news_2018_04:

===========================
Actions/news avril 2018
===========================





Les slides de Petazzoni pour les formations docker et kubernetes d'avril 2018
===============================================================================

- https://avril2018.container.training/
- https://avril2018.container.training/intro.yml.html#1 (Introduction to containers, 662 slides)
- https://avril2018.container.training/kube.yml.html#1 (introduction to orchtestration with kubernetes, 384 slides)

Le répertoire source des slides
---------------------------------

- https://github.com/jpetazzo/container.training


Autres conférences passées et futures
--------------------------------------

- http://container.training/


Docker for the busy researcher (from Erik Matsen)
===================================================

.. seealso::

   - http://erick.matsen.org/2018/04/19/docker.html


Why Docker ?
--------------

Have you ever been frustrated because a software package’s installation
instructions were incomplete ?
Or have you wanted to try out software without going through a complex
installation process?
Or have you wanted to execute your software on some remote machine in a
defined environment?


Docker can help.

In my group, we use Docker to make sure that our code compiles properly
in a defined environment and analyses are reproducible.
We automatically create Docker images through Dockerfiles. This provides
a clear list of dependencies which are guaranteed to work starting from
a defined starting point.

Once a Docker image is built, it can be run anywhere that runs the
Docker engine.
