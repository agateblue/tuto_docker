
.. index::
   pair: docker swarm; commands

.. _docker_swarm_commands:

==============================
**docker swarm**
==============================

- https://docs.docker.com/engine/reference/commandline/swarm/


.. _docker_swarm_init:

docker swarm init
==================

Description
--------------


**Initialize a swarm**

The client and daemon API must both be at least 1.24 to use this command.

Use the docker version command on the client to check your client and
daemon API versions.

Swarm This command works with the Swarm orchestrator.



.. _docker_swarm_join:

docker swarm join
==================

Description
----------------

**Join a swarm as a node and/or manager**

The client and daemon API must both be at least 1.24 to use this command.
Use the docker version command on the client to check your client and
daemon API versions.


.. _docker_swarm_join_token:

docker swarm join-token
==========================

Description
--------------

**Manage join tokens**

The client and daemon API must both be at least 1.24 to use this command.

Use the docker version command on the client to check your client and
daemon API versions.


.. _docker_swarm_leave:

docker swarm leave
=====================

Description
-------------

**Leave the swarm**

The client and daemon API must both be at least 1.24 to use this command.
Use the docker version command on the client to check your client and
daemon API versions.

Swarm This command works with the Swarm orchestrator.


.. _docker_swarm_unlock:

docker swarm unlock
=====================

Description
--------------

**Unlock swarm**

The client and daemon API must both be at least 1.24 to use this command.
Use the docker version command on the client to check your client and
daemon API versions.



.. _docker_swarm_update:

docker swarm update
=====================

Description
-------------

**Update the swarm**

The client and daemon API must both be at least 1.24 to use this command.
Use the docker version command on the client to check your client and daemon API versions.

Swarm This command works with the Swarm orchestrator.
